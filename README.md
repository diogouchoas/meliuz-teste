# DevOps AWS - Meliuz

Teste build jenkins...
Step 2 - Test

> Teste para vaga de Devops da Meliuz. Foi construído um template usando Cloudformation para criação do ambiente proposto.

Achei que seria muito complicado para automatizar a criação deste ambiente usando shell script puro, portanto preferi buscar uma ferramenta própria para este propósito, para facilitar os trabalhos. Preferi usar o cloudformation pois após uma pesquisa rápida das documentações, considerei a curva de aprendizado dele menor do que seria com outras ferramentas como ansible, puppet, etc. Criei primeiro a automação com 2 arquivos, um para o ambiente Python e outro para o PHP. Dessa forma era necessário executar o comando de criação para o primeiro ambiente, esperar o término da criação, buscar a URL do ELB no output do Stack, e inserir um novo comando para criação do próximo ambiente usando esta URL como parâmetro. Pesquisei mais um pouco e descobri a funcionalidade de Nested Stacks, que permite automatizar também este processo, então criei o terceiro arquivo que chamava sozinho os demais. Com esta primeira parte criada e funcionando, comecei a estudar sobre o CodeDeploy para já deixar o ambiente preparado para realizar deploys também automatizados, mas infelizmente não consegui entender direito como o processo de publicação funcionava neste serviço, e como o meu prazo estava terminando, acabei abortando a implementação do serviço.

 - Este template é composto por 3 arquivos:
	 - master-cf.json - Template principal. Usa Nested Stacks para criação de outros dois Stacks, um da API python e um da aplicação PHP.
	 - python-cf.json - Template para criação da estrutura Python
	 - php-cf.json - Template para criação da estrutura PHP



> Para uso da função de Nested Stacks, é necessário hospedar os templates no S3, por isso os arquivos estão apontando para o endereço https://s3-sa-east-1.amazonaws.com/devops-meliuz/, bucket onde eu já hospedei os templates para teste. Caso deseje utilizar estes templates em outro bucket, é necessário editar o endereço na chamada dos dois templates (PHP / Python) no template principal (master-cf.json).

```
	"TemplateURL":"https://s3-sa-east-1.amazonaws.com/devops-meliuz/python-cf.json",

	"TemplateURL":"https://s3-sa-east-1.amazonaws.com/devops-meliuz/php-cf.json",

```

#### Requisitos:
 - awscli - http://docs.aws.amazon.com/pt_br/cli/latest/userguide/installing.html


#### Parâmetros:
 - Instance - Tipo de Instância que será usada para criação das instâncias EC2. Exemplo: t2.large
 - KeyPairName - Nome do par de chave SSH que será usada para conexão às instâncias.
 - Email - Email que irá receber as notificações do SNS à respeito das stacks.
 - SSH - Faixa de rede que terá permissão de acesso à porta de SSH (TCP 22). Exemplo: 172.31.0.25/32


#### Sintaxe:

Criar stack:
```
aws --region sa-east-1 cloudformation create-stack --stack-name <nome do stack> \
    --template-url <url do template master-cf.json> \
    --parameters ParameterKey=Instance,ParameterValue=<tipo de instancia> \
     ParameterKey=KeyPairName,ParameterValue=<nome do par de chave> \
     ParameterKey=Email,ParameterValue=<email do operador> \
     ParameterKey=SSH,ParameterValue=<faixa de rede>
```

Monitorar eventos da criação do stack:
```
watch -n5 aws --region sa-east-1 cloudformation describe-stack-events --stack-name <nome do stack> --max-items 2
```

Buscar URL das aplicações (Após o processo de criação terminar):
```
aws --region sa-east-1 cloudformation describe-stacks --stack-name <nome do stack> |grep OutputValue
```

Excluir stack:
```
aws --region sa-east-1 cloudformation delete-stack --stack-name <nome do stack>
```

#### Exemplo:

Criar stack:
```
aws --region sa-east-1 cloudformation create-stack --stack-name meliuz \
    --template-url https://s3-sa-east-1.amazonaws.com/devops-meliuz/master-cf.json \
    --parameters ParameterKey=Instance,ParameterValue=t2.nano \
     ParameterKey=KeyPairName,ParameterValue=rsameliuz \
     ParameterKey=Email,ParameterValue=devops@meliuz.com.br \
     ParameterKey=SSH,ParameterValue=172.31.0.0/24
```

Monitorar eventos da criação do stack:
```
watch -n5 aws --region sa-east-1 cloudformation describe-stack-events --stack-name meliuz --max-items 2
```

Buscar URL das aplicações (Após o processo de criação terminar):
```
aws --region sa-east-1 cloudformation describe-stacks --stack-name meliuz |grep OutputValue
```

Excluir stack:
```
aws --region sa-east-1 cloudformation delete-stack --stack-name meliuz
```
